fsiInterFoam
============

## General

fsiInterFoam and fsiOverInterDyMFoam allow a variable number of subiteration
(`nOuterCorrectors` in system/fvSolution) during every time step to find 
a converged solution between the flow field  around and the motion of a 
freely moving (floating) body.

The solver uses the fsiSixDoFRigidBodyMotion library which is available
for download at <https://gitlab.com/brecht.devolder/fsiSixDoFRigidBodyMotion>.

## History

- Developed initially for **OpenFOAM-v1806**
- Upgraded to **OpenFOAM-v1812** on 17-JAN-2019

## References and citing

The model has been developed within: 
- the PhD thesis of Brecht Devolder at the Department of Civil Engineering at 
Ghent University and KU Leuven, funded by the Research Foundation – 
Flanders (FWO), Belgium (Ph.D. fellowship 1133817N). The PhD thesis is available 
for download at: <https://biblio.ugent.be/publication/8564551>. 
- the postdoctoral research project of Brecht Devolder at the department 
of civil engineering at KU Leuven, campus Bruges. The project is a collaboration 
between KU Leuven and DEME and is funded by the agency Flanders Innovation 
& Entrepreneurship (VLAIO) and DEME.

If you want to reference the model in your publications, you can use the 
following references in which the implementation and validation details 
are published:  
- Devolder, B. (2018). Hydrodynamic Modelling of Wave Energy Converter Arrays. PhD thesis. Ghent University, Faculty of Engineering and Architecture, Ghent, Belgium. KU Leuven, Faculty of Engineering Technology, Bruges, Belgium.
- Devolder, B., Troch, P., & Rauwoens, P. (2019). Accelerated numerical simulations of a heaving floating body by coupling a motion solver with a two-phase fluid solver. Computers and Mathematics with Applications, 77(6), 1605–1625. [doi:10.1016/j.camwa.2018.08.064](https://doi.org/10.1016/j.camwa.2018.08.064).
- Devolder, B., Stempinski, F., Mol, A., Rauwoens, P. (2019). Validation of a roll decay test of an offshore installation vessel using OpenFOAM. International Conference on Computational Methods in Marine Engineering (pp. 658–669). Presented at the International Conference on Computational Methods in Marine Engineering (MARINE 2019).

## Installation for OpenFOAM ESI OpenCFD releases

- Open a linux terminal and download the package using git:

      git clone https://gitlab.com/brecht.devolder/fsiInterFoam.git
      cd fsiInterFoam

- Source the OpenFOAM environment: 

      source $HOME/OpenFOAM/OpenFOAM-vYYMM/etc/bashrc        
      
- Compile the source code to build all solvers:

      wmake all

## How to use

Use the fsiInterFoam or fsiOverInterDyMFoam solver instead of interFoam
or overInterDyMFoam. Also modify constant/dynamicMeshDict as described at
<https://gitlab.com/brecht.devolder/fsiSixDoFRigidBodyMotion#how-to-use>.

## Contact

Brecht Devolder (PhD, MSc)  
Department of Civil Engineering - Construction TC  
KU Leuven - Campus Bruges  
Spoorwegstraat 12, B-8200 Brugge, Belgium  
<brecht.devolder@kuleuven.be>  
